﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataTest : MonoBehaviour
{
	class Enemy
	{
		public int Health { get; set; }
	}
	private void Start()
	{
		DataLoader dataLoader = ServiceLocator.Get<DataLoader>();
		JsonDataSource source = (JsonDataSource)dataLoader.LoadedDataSources["JsonTest"];

		foreach (KeyValuePair<string, object> kvp in source.DataDictionary)
		{
			Debug.Log(string.Format("<color=cyan>KEY:{0} VALUE:{1}</color>", kvp.Key, kvp.Value));
		}

		Enemy e = new Enemy();
		e.Health = System.Convert.ToInt32(source.DataDictionary["number"]);
		Debug.Log("Enemy Health is : " + e.Health);
	}


}
