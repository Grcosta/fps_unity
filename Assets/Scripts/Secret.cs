﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Secret : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
	}
	

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }
}
