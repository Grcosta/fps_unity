﻿using UnityEngine;

public class GunScript : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;

    public int maxAmmoRifle = 100;
    public int maxAmmoShotgun = 25;
    public int maxAmmoPistol = 50;

    public float reloadTimeRifle = 2f;
    public float reloadTimeShotgun = 3f;
    public float reloadTimePistol = 1f;


    //For the Raycasting
    public Camera fpsCamera;

    public ParticleSystem muzzleFlash;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
            
        }

    }

    void Shoot()
    {
        RaycastHit hit;

        AmmoManager.instance.Fire();
        //Debug.Log(AmmoManager.instance.currentAmmo.ToString());

        if(Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, range))
        {
            muzzleFlash.Play();
            gameObject.GetComponent<AudioSource>().Play();
            
            Debug.Log(hit.transform.name);
            DestructObjects target = hit.transform.GetComponent<DestructObjects>();
            if(target != null)
            {
                target.TakeDamage(damage);
            }
        }


    }
}
