﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PursuitPlayer : MonoBehaviour
{
    NavMeshAgent agent;
    public Animator animParasite;
    public Transform playerTransform;

    public float parasiteSpeed = 2.5f;
    public float distanceToDetect = 15f;
    public float attackRange = 2f;

    //Animation States
    private bool isIdle = false;
    private bool isRunning = false;
    private bool isAttacking = false;
    private bool isScreaming = false;
    private bool isDying = false;

    // Bug fix: Control the sliding death
    public float acceleration = 2.0f;
    public float deceleration = 100.0f;
    public


    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        isIdle = true;
        animParasite.SetBool("isIdle", true);
    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();
        ChasePlayer();
        ZombieDeath();

        if (agent)
        {
            if (agent.hasPath && isDying)
            {
                agent.acceleration = (agent.remainingDistance < attackRange) ? deceleration : acceleration;
            }
        }

        if (isAttacking)
        {
            agent.isStopped = true;
        }
    }

    public void FindPlayer()
    {
        NavMeshHit target = new NavMeshHit();
        agent.Raycast(playerTransform.position, out target);

        if (target.distance < distanceToDetect && target.distance > 2f)
        {
            isIdle = false;
            isRunning = true;
            isAttacking = false;
            isScreaming = false;
            animParasite.SetBool("isIdle", false);
            animParasite.SetBool("isRunning", true);
            animParasite.SetBool("isAttacking", false);
            animParasite.SetBool("isScreaming", false);
            ChasePlayer();
        }

        else if (target.distance < attackRange)
        {
            isIdle = false;
            isRunning = false;
            isAttacking = true;
            isScreaming = false;
            animParasite.SetBool("isIdle", false);
            animParasite.SetBool("isRunning", false);
            animParasite.SetBool("isAttacking", true);
            animParasite.SetBool("isScreaming", false);
            AttackPlayer();
        }

        else
        {
            isIdle = true;
            isRunning = false;
            isAttacking = false;
            isScreaming = false;
            animParasite.SetBool("isIdle", true);
            animParasite.SetBool("isRunning", false);
            animParasite.SetBool("isAttacking", false);
            animParasite.SetBool("isScreaming", false);
            agent.isStopped = true;
        }
    }

    void ChasePlayer()
    {
        if (isRunning)
        {
            agent.isStopped = false;
            agent.speed = parasiteSpeed;
            agent.SetDestination(playerTransform.position);
        }
    }

    void AttackPlayer()
    {
        if (isAttacking)
        {
            agent.isStopped = true;
            agent.speed = 0f;
            agent.acceleration = 100f;
            agent.SetDestination(gameObject.transform.position);
        }
    }

    void ZombieDeath()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            agent.isStopped = true;
            isDying = true;
            animParasite.SetTrigger("isDying");
            Destroy(gameObject, 5f);
        }
    }
}
