﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health;

    public void TakeDamage(int amount)
    {
        health -= amount;

        if (health < 1)
        {
            Die();
        }
    }

    public void Die()
    {
        //PursuitPlayer.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Projectile"))
        {
            TakeDamage(25);
            Destroy(other.gameObject);
        }
    }


}
