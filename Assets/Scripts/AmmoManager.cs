﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoManager : MonoBehaviour
{
    public static AmmoManager instance;

    public Text remainingAmmo;      //Text information on UI display - currentAmmo
    public Text totalAmmo;          //Text information on UI display - maxAmmo
    
    public int currentAmmo;         //Number information for the ammo math
    public int maxAmmo;             //Number information for the max ammo math


    public void CheckLowAmmo()     //Function to clamp the number of bullets in 0 when depleted;
    {
        if(currentAmmo < 0)
        {
            currentAmmo = 0;
        }
    }

    public int Fire()
    {
        if (currentAmmo != 0)
            currentAmmo--;
        else
            currentAmmo = 0;

        return currentAmmo;
    }


    // Use this for initialization
    void Start()
    {
        if (instance == null)
            instance = this;
        if (instance != null)
            Destroy(this);



        currentAmmo = 15;

    }

    // Update is called once per frame
    void Update()
    {
        remainingAmmo.text = currentAmmo.ToString();

        CheckLowAmmo();
    }


}
