﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject bullet;
    public Rigidbody projectile;
    public Transform rifle_nozzle;
    public float shotForce = 1000f;

	// Use this for initialization
	void Start ()
    {
        projectile = bullet.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Rigidbody shot = Instantiate
                (
                projectile,
                rifle_nozzle.position,
                rifle_nozzle.rotation
                ) as Rigidbody;

            shot.AddForce(rifle_nozzle.forward * shotForce);
        }
	}
}
